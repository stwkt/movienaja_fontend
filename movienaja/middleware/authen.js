import Vue from 'vue'

export default function IsAuthen({
    next,
    router
}) {
    const cookies = Vue.prototype.$cookies
    if (!cookies.get('authenticated')) {
        return router.push({
            path: '/'
        })
    } else {
        return next()
    }
}