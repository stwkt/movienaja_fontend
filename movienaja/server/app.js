const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')

// const uri = 'mongodb://localhost/movienaja';
// const db = mongoose.connect(url);

const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(cors())
app.use(bodyParser.urlencoded({
    extended: true
}))
const Schema = new mongoose.Schema({
    title: String,
    detail: String
});
const server = app.listen(8080, function() {
    console.log('server running on port 8080')
});


const io = require('socket.io')(server)
io.on('connection', function(socket) {
    console.log(socket.id)
    socket.on('SEND_MESSAGE', function(data) {
        io.emit('MESSAGE', data)
    });
});