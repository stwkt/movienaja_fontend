import Vue from 'vue'
import Vuex from 'vuex'
import pathify, {
    make
} from 'vuex-pathify'
import VueSession from 'vue-session'
Vue.use(VueSession)

const state = {
    items: [],
    items_user: [],
    items_movie: [],
    items_seat: [],
}
const mutations = make.mutations(state)
Vue.use(Vuex)

export default new Vuex.Store({
    plugins: [
        pathify.plugin
    ],
    state,
    mutations
})