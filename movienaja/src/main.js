import Vue from 'vue'
import './plugins/vuetify'
// import Vuetify from 'vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import axios from 'axios'
import VueAxios from 'vue-axios'
import StarRating from 'vue-star-rating'
import Cookies from 'universal-cookie'
import BootstrapVue from 'bootstrap-vue'
import VueChatScroll from 'vue-chat-scroll'
import svg from 'vue-svg-directive'

import {
    library
} from '@fortawesome/fontawesome-svg-core'
import {
    faUserSecret
} from '@fortawesome/free-solid-svg-icons'
import {
    FontAwesomeIcon
} from '@fortawesome/vue-fontawesome'

library.add(faUserSecret)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(svg, {
    filepath: '/static/sprites.svg', // Optional filepath to your svg sprite
    prefix: 'icon-', // Optional prefix for all your sprite names
    class: 'icon', // Optional class that will be applied to the <svg> element (optional)
    size: '32' // Optional default size value
})


Vue.use(VueChatScroll)
Vue.use(BootstrapVue)
Vue.use(VueAxios, axios)
Vue.config.productionTip = false
Vue.component('star-rating', StarRating)

// Global Variable
Vue.prototype.$cookies = new Cookies()

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')