import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import Toolbar from './views/Toolbar.vue'
import Dashboard from './views/Dashboard.vue'
import register from './views/Register.vue'
import Moredetail from './views/MoreDetail.vue'
import Booking from '@/views/Booking.vue'
import DashboardForm from '@/components/DashboardForm.vue'
import MovieForm from '@/components/MovieForm.vue'
import Chat from '@/components/Chat.vue'
import Ticket from './views/Ticket.vue'
// middleware
// import IsAuthen from '../middleware/authen'
Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        // {
        //     path: '/',
        //     name: 'Login',
        //     component: Login
        // },
        {
            path: '/register',
            name: 'register',
            component: register
        },
        {
            path: '/',
            name: 'Toolbar',
            component: Toolbar,
            children: [{
                    path: '',
                    name: 'Dashboard',
                    component: Dashboard,
                    //   meta: {
                    //     middleware: [IsAuthen]
                    //   }

                },
                {
                    path: '/moredetail',
                    name: 'Moredetail',
                    component: Moredetail,
                    // meta: {
                    //     middleware: [IsAuthen]
                    // }
                },
                {
                    path: '/chat',
                    name: 'Chat',
                    component: Chat
                },
                {
                    path: '/DashboardForm',
                    name: 'DashboardForm',
                    component: DashboardForm,
                    // meta: {
                    //     middleware: [IsAuthen]
                    // }
                },
                {
                    path: '/MovieForm',
                    name: 'MovieForm',
                    component: MovieForm,
                    // meta: {
                    //     middleware: [IsAuthen]
                    // }
                },
                {
                    path: '/register',
                    name: 'register',
                    component: register,
                    // meta: {
                    //     middleware: [IsAuthen]
                    // }
                }, {
                    path: '/booking',
                    name: 'Booking',
                    component: Booking,
                    // meta: {
                    //     middleware: [IsAuthen]
                    // }
                },
                {
                    path: '/ticket',
                    name: 'Ticket',
                    component: Ticket,

                },
                {
                    path: '/chat',
                    name: 'Chat',
                    component: Chat,

                }

            ]
        }

    ]
})

function nextFactory(context, middleware, index) {
    const subsequentMiddleware = middleware[index]
    if (!subsequentMiddleware) return context.next

    return (...parameters) => {
        context.next(...parameters)
        const nextMiddleware = nextFactory(context, middleware, index + 1)
        subsequentMiddleware({
            ...context,
            next: nextMiddleware
        })
    }
}
// var router = this.$router
router.beforeEach((to, from, next) => {
    if (to.meta.middleware) {
        const middleware = Array.isArray(to.meta.middleware) ?
            to.meta.middleware : [to.meta.middleware]

        const context = {
            from,
            next,
            router,
            to
        }
        const nextMiddleware = nextFactory(context, middleware, 1)

        return middleware[0]({
            ...context,
            next: nextMiddleware
        })
    }

    return next()
})
export default router